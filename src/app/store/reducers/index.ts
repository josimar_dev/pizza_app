import {
  ActionReducerMap,
  createFeatureSelector,
  MetaReducer,
} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import {
  ActivatedRouteSnapshot,
  Params,
  RouterStateSnapshot,
} from '@angular/router';
import { hydrationMetaReducer } from './hydration.reducer';

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  routerReducer: fromRouter.routerReducer,
};

export const getRouterState = createFeatureSelector<
  fromRouter.RouterReducerState<RouterStateUrl>
>('routerReducer');

/**
 * CUSTOM SERIALIZER FUNCTION
 *
 * The Store from NGRX will keep listening to any changes in the URL,
 * which means it will keep tracking of the state of the Angular Router
 * Everytime you navigate to a different route, the Custom Serializer function
 * will be called, and since it's bound to the NGRX's State, the state tracking
 * url changes will be uptaded accordingly in the Store.
 */

export class CustomSerializer
  implements fromRouter.RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;

    // The Router is a state tree itself, which means we have to
    // traverse it manually
    // This is the state tree of the Angular Router,
    // It has nothing to do with the state from NGRX
    // We have to manually extract the properties we need from the router and bind them
    // to our Store
    let state: ActivatedRouteSnapshot = routerState.root;

    // state.firstChild = domain/whatever ( 'whatever' could be any valid route like /products )
    while (state.firstChild) {
      state = state.firstChild;
    }

    const { params } = state;

    // This is the object that's going to be bound to the State Tree
    return { url, queryParams, params };
  }
}

export const metaReducers: MetaReducer[] = [hydrationMetaReducer];
