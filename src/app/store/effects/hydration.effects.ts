// hydration.effects.ts
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import * as HydrationActions from '../actions/hydration.actions';

@Injectable()
export class HydrationEffects implements OnInitEffects {
  /**
   * hydrate$ effect dispatches a HYDRATE_SUCCESS actions based on
   * whether there's a state saved in the localstorage, otherwise,
   * it dispatches a HYDRATE_FAILURE action
   */
  hydrate$ = createEffect(() =>
    this.action$.pipe(
      ofType(HydrationActions.HYDRATE),
      map(() => {
        const storageValue = localStorage.getItem('state');
        if (storageValue) {
          try {
            const state = JSON.parse(storageValue);
            return HydrationActions.HYDRATE_SUCCESS({ state });
          } catch {
            localStorage.removeItem('state');
          }
        }
        return HydrationActions.HYDRATE_FAILURE();
      })
    )
  );

  serialize$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(
          HydrationActions.HYDRATE_SUCCESS,
          HydrationActions.HYDRATE_FAILURE
        ),
        switchMap(() => this.store),
        distinctUntilChanged(),
        tap((state) => localStorage.setItem('state', JSON.stringify(state)))
      ),
    { dispatch: false }
  );

  constructor(private action$: Actions, private store: Store<any>) {}

  /**
   * ngrxOnInitEffects runs after the effects are added, it works exaclty
   * like the ngOnInit method for components life cycles does
   */
  ngrxOnInitEffects(): Action {
    return HydrationActions.HYDRATE();
  }
}
