import { Injectable } from '@angular/core';

import { createEffect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import * as toppingsActions from '../actions/toppings.action';
import { of } from 'rxjs';
import { ToppingService } from '../../services';

@Injectable()
export class ToppingsEffect {
  constructor(
    private actions$: Actions,
    private toppingsService: ToppingService
  ) {}

  loadToppings$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Products] Load Toppings'),
      mergeMap(() =>
        this.toppingsService.getToppings().pipe(
          map((toppings) =>
            toppingsActions.LOAD_TOPPINGS_SUCCESS({ payload: toppings })
          ),
          catchError((error) =>
            of(toppingsActions.LOAD_TOPPINGS_FAIL({ payload: error }))
          )
        )
      )
    )
  );
}
