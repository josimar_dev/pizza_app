import { createReducer, on, Action } from '@ngrx/store';
import {
  LOAD_PIZZAS,
  LOAD_PIZZAS_FAIL,
  LOAD_PIZZAS_SUCCESS,
} from '../actions/pizzas.action';

import { Pizza } from '../../models/pizza.model';
import { reducesArrayToEntity } from '../../../utils';

export interface PizzaState {
  entities: { [id: number]: Pizza };
  loaded: boolean;
  loading: boolean;
}

export const initialState: PizzaState = {
  entities: {},
  loaded: false,
  loading: false,
};

const _pizzaReducer = createReducer(
  initialState,
  on(LOAD_PIZZAS, (state) => ({ ...state, loading: true })),
  on(LOAD_PIZZAS_FAIL, (state, { payload }) => ({
    ...state,
    loaded: false,
    loading: false,
  })),
  on(LOAD_PIZZAS_SUCCESS, (state, { payload }) => {
    // We're transforming the array of pizzas into an entities object
    // Every single id of this object points to a particular pizza
    // Working with entities/objects is faster than doing ut with arrays since
    // you don't have to iterate over a collection to find an item,
    // you just have to use the id of a specific entity and that's it.

    const entities = reducesArrayToEntity(payload, state.entities);

    console.log(entities);

    return {
      ...state,
      loading: false,
      loaded: true,
      entities: { ...entities },
    };
  })
);

// Functions to compose and pass into the createSelector method
// They allow us to select a specif property of the state and return its value
export const getPizzasLoading = (state: PizzaState) => state.loading;
export const getPizzasLoaded = (state: PizzaState) => state.loaded;
export const getPizzasEntities = (state: PizzaState) => state.entities;

export function pizzaReducer(state: PizzaState, action: Action) {
  return _pizzaReducer(state, action);
}
