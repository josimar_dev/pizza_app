import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromPizzas from './pizzas.reducer';
import * as fromToppings from './toppings.reducer';

export interface ProductsState {
  pizzas: fromPizzas.PizzaState;
  toppings: fromToppings.ToppingsState;
}

export const reducers: ActionReducerMap<ProductsState> = {
  pizzas: fromPizzas.pizzaReducer,
  toppings: fromToppings.toppingsReducer,
};

// Selector for this module - It selects the product module itself
// createFeatureSelector - allows us to select the whole module as a reference for selectors.
export const getProductsState = createFeatureSelector<ProductsState>(
  'products'
);
