import { PizzasService } from './pizza.service';
import { ToppingService } from './topping.service';

export const services: any[] = [PizzasService, ToppingService];

export * from './pizza.service';
export * from './topping.service';
