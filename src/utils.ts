export const reducesArrayToEntity = (
  array: Array<any>,
  stateEntities: object
): object => {
  const entities = array.reduce(
    (acc, item) => {
      return {
        ...acc,
        [item.id]: item,
      };
    },
    {
      ...stateEntities,
    }
  );

  return entities;
};
