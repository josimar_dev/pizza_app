# LearningNgrx

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Node Packages

Run `npm install` to install all the required dependencies for this project

## JSON Server API

Run `json-server --watch db.json` to run a local server on port 3000 that retrieves the API for this application

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
